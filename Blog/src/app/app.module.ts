import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PostListComponent } from './post-list/post-list.component';
import { PostService } from './services/post.service';
import {RouterModule, Routes} from '@angular/router';
import { SinglePostComponent } from './single-post/single-post.component';
import { PostViewComponent } from "./post-view/post-view.component";
import { NewPostComponent } from './new-post/new-post.component';


const appRoutes: Routes = [
    { path: 'posts', component: PostViewComponent },
    { path: 'posts/:id', component: SinglePostComponent },
    { path: '', component: PostViewComponent }

];

@NgModule({
  declarations: [
    AppComponent,
    PostListComponent,
    SinglePostComponent,
    PostViewComponent,
    NewPostComponent

  ],
  imports: [
    BrowserModule,
      RouterModule.forRoot(appRoutes)

  ],
  providers: [
    PostService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
