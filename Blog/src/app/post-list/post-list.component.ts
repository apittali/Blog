import {Component, Input, Output, OnInit, EventEmitter} from '@angular/core';
import {PostService} from "../services/post.service";


@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.scss']
})
export class PostListComponent implements OnInit {

    @Input () postName: string;
    @Input () postContent: string;
    @Input () index: number;
    @Input () loveIts: number;
    @Input () id: number;

    btnlove: string = 'Love it!';
    btndont: string = "Don't love it!"
    btndelete: string = 'Delete'


  constructor(private postService:PostService) { }

    ngOnInit() {
    }

    lastUpdate = new Promise((resolve, reject) => {
        const date = new Date();
        setTimeout(
            () => {
                resolve(date);
            }, 2000
        );
    });

    onLove() {
        this.postService.setLoveIts(this.index, this.loveIts + 1)
    }

    onDont() {
        this.postService.setDont(this.index, this.loveIts - 1);
    }

    onDelete(id) {
        this.postService.delete(id);
        console.log(id);
    }


}
