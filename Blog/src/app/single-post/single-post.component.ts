import {Component, Input, OnInit} from '@angular/core';
import {PostService} from "../services/post.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss']
})


export class SinglePostComponent implements OnInit {

    btnlove: string = 'Love it!';
    btndont: string = "Don't love it!";
    btndelete: string = 'Delete';

    @Input () postName: string;
    @Input () postContent: string;
    @Input () index: number;
    @Input () loveIts: number = 0;
    @Input () id: number;

  constructor(private postService: PostService, private route: ActivatedRoute) { }

  ngOnInit() {
      const id = this.route.snapshot.params['id'];
      this.postName = this.postService.getPostsById(+id).name;
      this.postContent = this.postService.getPostsById(+id).content;
      this.loveIts = this.postService.getPostsById(+id).loveIts;
  }


    onLove() {
        this.postService.setLoveIts(this.index, this.loveIts + 1)
    }

    onDont() {
        this.postService.setDont(this.index, this.loveIts - 1);
    }

    onDelete(id) {
        this.postService.delete(id);
        console.log(id);
    }
}
