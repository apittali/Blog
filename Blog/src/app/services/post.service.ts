import { Injectable, Input } from '@angular/core';

@Injectable()
export class PostService {

    @Input () postName: string;
    @Input () postContent: string;
    @Input () index: number;
    @Input () loveIts: number;
    @Input () id: number;

    btnlove: string = 'Love it!';
    btndont: string = "Don't love it!"
    btndelete: string = 'Delete'
    public posts =[
        {
            id: 1,
            name: 'Mon premier post',
            content: 'Ipsam vero urbem Byzantiorum fuisse refertissimam atque ornatissimam signis quis ignorat? Quae illi, exhausti sumptibus bellisque maximis, cum omnis Mithridaticos impetus totumque Pontum armatum affervescentem in Asiam atque erumpentem, ore repulsum et cervicibus interclusum suis sustinerent, tum, inquam, Byzantii et postea signa illa et reliqua urbis ornanemta sanctissime custodita tenuerunt.',
            loveIts: 0
        },
        {
            id: 2,
            name: 'Mon second post',
            content: 'Ipsam vero urbem Byzantiorum fuisse refertissimam atque ornatissimam signis quis ignorat? Quae illi, exhausti sumptibus bellisque maximis, cum omnis Mithridaticos impetus totumque Pontum armatum affervescentem in Asiam atque erumpentem, ore repulsum et cervicibus interclusum suis sustinerent, tum, inquam, Byzantii et postea signa illa et reliqua urbis ornanemta sanctissime custodita tenuerunt.',
            loveIts: 0
        },
        {
            id: 3,
            name: 'Une autre post',
            content: 'Ipsam vero urbem Byzantiorum fuisse refertissimam atque ornatissimam signis quis ignorat? Quae illi, exhausti sumptibus bellisque maximis, cum omnis Mithridaticos impetus totumque Pontum armatum affervescentem in Asiam atque erumpentem, ore repulsum et cervicibus interclusum suis sustinerent, tum, inquam, Byzantii et postea signa illa et reliqua urbis ornanemta sanctissime custodita tenuerunt.',
            loveIts: 0
        }
    ];

  constructor() { }

    getPostsById(id: number) {
        const post = this.posts.find(
            (s) => {
                return s.id === id;
            }
        );
        return post;
    }

    setLoveIts(index, value) {
      this.posts[index].loveIts = value;
    }

    setDont(index, value) {
        this.posts[index].loveIts = value;
    }


    addPost(name: string, content: string, loveIts: number) {
        const postObject = {
            id: 0,
            name: '',
            date: new Date(),
            content: '',
            loveIts: 0

        };
        postObject.name = name;
        postObject.content = content;
        postObject.id = this.posts[(this.posts.length - 1)].id + 1;
        this.posts.push(postObject);
    }
    delete(id) {
        const index = this.posts.map(function(x) { return x.id; }).indexOf(id);
        this.posts.splice(index, 1);
    }


}
